package com.denisk.wordlearn.dto;

import com.denisk.wordlearn.model.Word;

/**
 * @author denisk
 * @since 6/18/14.
 */
public class WordDTO extends Word {
	public String translation;

	public WordDTO() {
	}

	public WordDTO(String key, int positiveAttempts, int negativeAttempts, int learnt) {
		//this is what is persisted from SDL for now
		this.key = key;
		this.positiveAttempts = positiveAttempts;
		this.negativeAttempts = negativeAttempts;
		this.learnt = learnt;
	}


}
