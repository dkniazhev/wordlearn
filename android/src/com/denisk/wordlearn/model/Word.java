package com.denisk.wordlearn.model;

import java.util.Date;

/**
 * @author denisk
 * @since 4/24/14.
 */
public class Word {
	public static final String TABLE_NAME = "words";

	public static final String FIELD_KEY = "key";
	public static final String FIELD_TEXT = "text";
	public static final String FIELD_LANG = "lang";
	public static final String FIELD_POSITIVE_ATTEMPTS = "positive_attempts";
	public static final String FIELD_NEGATIVE_ATTEMPTS = "negative_attempts";
	public static final String FIELD_CREATED = "created";
	public static final String FIELD_UPDATED = "updated";
	public static final String FIELD_BADGE_IDS = "badge_ids";
	public static final String FIELD_LEARNT = "learnt";

	public static final String LANG_EN = "EN";
	public static final String LANG_RU = "RU";

	/**
	 * Key should be hash of text+lang
	 */
	public String key;
	public String text;
	public String lang;
	public int positiveAttempts;
	public int negativeAttempts;
	public Date created;
	public Date updated;
	public String badgeIds;
	public int learnt;

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Word{");
		sb.append("key='").append(key).append('\'');
		sb.append(", text='").append(text).append('\'');
		sb.append(", lang='").append(lang).append('\'');
		sb.append(", positiveAttempts=").append(positiveAttempts);
		sb.append(", negativeAttempts=").append(negativeAttempts);
		sb.append(", created=").append(created);
		sb.append(", updated=").append(updated);
		sb.append(", badgeIds='").append(badgeIds).append('\'');
		sb.append(", learnt=").append(learnt);
		sb.append('}');
		return sb.toString();
	}
}
