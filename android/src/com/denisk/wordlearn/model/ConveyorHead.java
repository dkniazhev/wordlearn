package com.denisk.wordlearn.model;

/**
 * @author denisk
 * @since 07.07.14.
 */
public class ConveyorHead {
	public static final String TABLE_NAME = "conveyor_head";

	public static final String FIELD_HEAD = "head";

	public int head;
}
