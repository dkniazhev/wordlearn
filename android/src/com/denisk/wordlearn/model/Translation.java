package com.denisk.wordlearn.model;

/**
 * @author denisk
 * @since 6/18/14.
 */
public class Translation {
	public static final String TABLE_NAME = "translations";

	public static final String FIELD_KEY1 = "key1";
	public static final String FIELD_KEY2 = "key2";

	public String key1;
	public String key2;
}
