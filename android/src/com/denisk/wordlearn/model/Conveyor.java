package com.denisk.wordlearn.model;

/**
 * @author denisk
 * @since 07.07.14.
 */
public class Conveyor {
	public static final String TABLE_NAME = "conveyor";

	public static final String FIELD_WORD_KEY = "word_key";

	public static final int CONVEYOR_LENGTH = 50;

	public String wordKey;
}
