package com.denisk.wordlearn.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.denisk.wordlearn.dto.WordDTO;
import com.denisk.wordlearn.model.Word;

import java.util.*;

/**
 * @author denisk
 * @since 6/18/14.
 */
public class WordManager {
	public SQLiteDatabase db;

	public WordManager(SQLiteDatabase db) {
		this.db = db;
	}

	public ArrayList<WordDTO> getWordsForKeys(List<String> keys) {
		ArrayList<WordDTO> wordDTOs = getUnsortedWordsForKeys(keys, 0, false); //limit 0, fetching all conveyor

		if (wordDTOs.size() != keys.size()) {
			throw new RuntimeException("Size of words " + wordDTOs.size() + " does not match the size of the keys " + keys.size());
		}
		HashMap<String, WordDTO> indexedWordsMap = new HashMap<>(keys.size());
		for (WordDTO w : wordDTOs) {
			indexedWordsMap.put(w.key, w);
		}
		ArrayList<WordDTO> sortedWords = new ArrayList<>();
		for (String key : keys) {
			sortedWords.add(indexedWordsMap.get(key));
		}
		return sortedWords;
	}

	public ArrayList<WordDTO> getUnlearntWords(int limit) {
		return getUnsortedWordsForKeys(new ArrayList<String>(), limit, true);
	}

	private ArrayList<WordDTO> getUnsortedWordsForKeys(List<String> keys, int limit, boolean excludeConveyor) {
		ArrayList<String> doubleKeys = new ArrayList<>(keys.size() * 2);
		//todo customize langs
		doubleKeys.addAll(keys);
		doubleKeys.addAll(keys);

		return getWordsForQuery(getWordsSql(Word.LANG_EN, Word.LANG_RU, keys.size(), limit, excludeConveyor), doubleKeys);
	}

	ArrayList<WordDTO> getWordsForQuery(String wordsQuery, List<String> keys) {
		Cursor c = null;
		ArrayList<WordDTO> words = new ArrayList<>(keys.size());
		try {
			c = db.rawQuery(wordsQuery, keys.toArray(new String[]{}));
			if (c.moveToFirst()) {
				do {
					words.add(fromCursor(c));
				} while(c.moveToNext());
			}
		} finally {
			if(c != null) {
				try {
					c.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return words;
	}

	public void closeDb() {
		db.close();
	}

	private String getWordsSql(String sourceLang, String destLang, int keysCount, int limit, boolean excludeConveyor) {
		StringBuilder whereKeyIn = new StringBuilder();
		if (keysCount > 0) {
			whereKeyIn.append(" WHERE w.key IN (");
			for(int i = 0; i < keysCount; i++) {
				whereKeyIn.append("?");
				if(i != keysCount - 1) {
					whereKeyIn.append(", ");
				}
			}
			whereKeyIn.append(")");
		}

		return "SELECT\n" +
				"  key,\n" +
				"  text,\n" +
				"  group_concat(translation),\n" +
				"  positive_attempts,\n" +
				"  negative_attempts,\n" +
				"  created,\n" +
				"  updated,\n" +
				"  badge_ids\n" +
				"  \n" +
				"FROM (\n" +
				"\n" +
				"  SELECT\n" +
				"    w.key,\n" +
				"    w.text,\n" +
				"    w.lang,\n" +
				"    wt.text AS translation,\n" +
				"    w.positive_attempts,\n" +
				"    w.negative_attempts,\n" +
				"    w.created,\n" +
				"    w.updated,\n" +
				"    w.badge_ids\n" +
				"  FROM words w INNER JOIN translations t ON (w.learnt <> 1 AND w.lang = '" + sourceLang + "' AND w.key = t.key1)\n" +
				"    INNER JOIN words wt ON (wt.lang = '" + destLang + "' AND t.key2 = wt.key)\n" +
					 whereKeyIn +
				"\n" +
				"  UNION\n" +
				"\n" +
				"  SELECT\n" +
				"    w.key,\n" +
				"    w.text,\n" +
				"    w.lang,\n" +
				"    wt.text AS translation,\n" +
				"    w.positive_attempts,\n" +
				"    w.negative_attempts,\n" +
				"    w.created,\n" +
				"    w.updated,\n" +
				"    w.badge_ids\n" +
				"  FROM words w INNER JOIN translations t ON (w.learnt <> 1 AND w.lang = '" + sourceLang + "' AND w.key = t.key2)\n" +
				"    INNER JOIN words wt ON (wt.lang = '" + destLang + "' AND t.key1 = wt.key)\n" +
					 whereKeyIn +
				")\n" +
				(excludeConveyor ? " LEFT OUTER JOIN conveyor c on c.word_key = key WHERE c.word_key IS NULL" : "") +
				" GROUP BY key" +
				(limit > 0 ? " LIMIT " + limit : "") +
				" ;";
	}

	/**
	 * Cursor must be obtained from getWordsSql method
	 */
	private static WordDTO fromCursor(Cursor c) {
		WordDTO w = new WordDTO();

		w.key = c.getString(0);
		w.text = c.getString(1);
		w.translation = c.getString(2);
		w.positiveAttempts = c.getInt(3);
		w.negativeAttempts = c.getInt(4);
		w.created = new Date(c.getLong(5));
		w.updated = new Date(c.getLong(6));
		w.badgeIds = c.getString(7);

		return w;
	}

	public void updateWord(WordDTO word) {
		ContentValues val = new ContentValues();
		val.put(Word.FIELD_POSITIVE_ATTEMPTS, word.positiveAttempts);
		val.put(Word.FIELD_NEGATIVE_ATTEMPTS, word.negativeAttempts);
		val.put(Word.FIELD_LEARNT, word.learnt);

		db.update(Word.TABLE_NAME, val, Word.FIELD_KEY + "=?" , new String[]{word.key});
	}
}
