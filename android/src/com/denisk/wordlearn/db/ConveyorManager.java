package com.denisk.wordlearn.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.denisk.wordlearn.model.Conveyor;
import com.denisk.wordlearn.model.ConveyorHead;

import java.util.ArrayList;
import java.util.List;

/**
 * @author denisk
 * @since 07.07.14.
 */
public class ConveyorManager {
	public SQLiteDatabase db;

	public ConveyorManager(SQLiteDatabase db) {
		this.db = db;
	}

	public void persistConveyorAndHead(List<String> wordKeys, int head) {
		db.beginTransaction();
		try {
			setConveyor(wordKeys);
			setConveyorHead(head);
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	public List<String> getKeysInConveyor() {
		Cursor cursor = db.query(Conveyor.TABLE_NAME, new String[]{Conveyor.FIELD_WORD_KEY}, null, null, null, null, null);
		ArrayList<String> result = new ArrayList<>(cursor.getCount());
		if (cursor.moveToFirst()) {
			do {
				result.add(cursor.getString(0));
			} while (cursor.moveToNext());
		}

		return result;
	}

	public int getHead() {
		Cursor cursor = db.query(ConveyorHead.TABLE_NAME, new String[]{ConveyorHead.FIELD_HEAD}, null, null, null, null, null, "1");
		if(! cursor.moveToFirst()) {
			throw new RuntimeException("No head");
		}
		return cursor.getInt(0);
	}

	private void setConveyor(List<String> wordKeys) {
		db.delete(Conveyor.TABLE_NAME, null, null);
		for(String key: wordKeys) {
			ContentValues v = new ContentValues();
			v.put(Conveyor.FIELD_WORD_KEY, key);
			db.insert(Conveyor.TABLE_NAME, null, v);
		}
	}

	private void setConveyorHead(int head) {
		db.delete(ConveyorHead.TABLE_NAME, null, null);

		ContentValues v = new ContentValues();
		v.put(ConveyorHead.FIELD_HEAD, head);
		db.insert(ConveyorHead.TABLE_NAME, null, v);
	}
}
