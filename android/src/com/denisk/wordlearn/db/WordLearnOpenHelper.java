package com.denisk.wordlearn.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.denisk.wordlearn.model.Conveyor;
import com.denisk.wordlearn.model.ConveyorHead;
import com.denisk.wordlearn.model.Translation;
import com.denisk.wordlearn.model.Word;

/**
 * @author denisk
 * @since 6/18/14.
 */
public class WordLearnOpenHelper extends SQLiteOpenHelper {

	public static final String DB_NAME = "wordlearn.db";

	private static final int DB_VERSION = 1;

	public WordLearnOpenHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.i("==", "onCreate");

		db.execSQL(
				"CREATE TABLE " + Word.TABLE_NAME + "(" +
				Word.FIELD_KEY + " TEXT PRIMARY KEY, " +
				Word.FIELD_TEXT + " TEXT, " +
				Word.FIELD_LANG + " TEXT, " +
				Word.FIELD_POSITIVE_ATTEMPTS + " INTEGER DEFAULT 0, " +
				Word.FIELD_NEGATIVE_ATTEMPTS + " INTEGER DEFAULT 0, " +
				Word.FIELD_CREATED + " INTEGER, " +
				Word.FIELD_UPDATED + " INTEGER, " +
				Word.FIELD_BADGE_IDS + " TEXT, " +
				Word.FIELD_LEARNT + " INTEGER DEFAULT 0); "
		);
		db.execSQL("CREATE TABLE " + Translation.TABLE_NAME + "(" +
				Translation.FIELD_KEY1 + " TEXT, " +
				Translation.FIELD_KEY2 + " TEXT);");
		db.execSQL("CREATE TABLE " + Conveyor.TABLE_NAME + "(" +
				Conveyor.FIELD_WORD_KEY + " TEXT);");
		db.execSQL("CREATE TABLE " + ConveyorHead.TABLE_NAME + "(" +
				ConveyorHead.FIELD_HEAD + " INTEGER);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.i("==", "onUpgrade");

		db.execSQL("DROP TABLE IF EXISTS " + Word.TABLE_NAME + ";");
		db.execSQL("DROP TABLE IF EXISTS " + Translation.TABLE_NAME + ";");
		db.execSQL("DROP TABLE IF EXISTS " + Conveyor.TABLE_NAME + ";");
		db.execSQL("DROP TABLE IF EXISTS " + ConveyorHead.TABLE_NAME + ";");

		onCreate(db);
	}
}
