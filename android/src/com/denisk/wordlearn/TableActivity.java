package com.denisk.wordlearn;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import com.denisk.wordlearn.db.ConveyorManager;
import com.denisk.wordlearn.db.WordLearnOpenHelper;
import com.denisk.wordlearn.db.WordManager;
import com.denisk.wordlearn.dto.WordDTO;
import com.denisk.wordlearn.model.Conveyor;
import com.denisk.wordlearn.model.ConveyorHead;
import com.denisk.wordlearn.model.Translation;
import com.denisk.wordlearn.model.Word;
import com.denisk.wordlearn.util.CSVReader;
import org.libsdl.app.SDLActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 * @author denisk
 * @since 4/25/14.
 */
public class TableActivity extends SDLActivity {
	public static final String FIRST_RUN_KEY = "been_here";
	private static WordManager wordManager;
	private static ConveyorManager conveyorManager;

	static {
		System.loadLibrary("AndroidTableWrapper");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		SharedPreferences preferences = getPreferences(MODE_PRIVATE);
		boolean firstRun = !preferences.contains(FIRST_RUN_KEY);
//		todo delete this
		if (firstRun) {
			getContext().deleteDatabase(WordLearnOpenHelper.DB_NAME);
		}

		initManagers();

		List<String> keysInConveyor = conveyorManager.getKeysInConveyor();
		ArrayList<WordDTO> words;
		int head;
		if (keysInConveyor.size() == 0) {
			//todo delete this
			File file = new File("/sdcard/words.csv");
			if (!file.exists()) {
				throw new RuntimeException("No words file");
			}
			CSVReader reader;
			try {
				reader = new CSVReader(new FileReader(file));
			} catch (FileNotFoundException e) {
				throw new RuntimeException("This should never happen");
			}
			String sourceLang = Word.LANG_EN;
			String translationLang = Word.LANG_RU;
			String[] next;
			HashSet<ContentValues> wordsSet = new HashSet<>();
			HashSet<ContentValues> translationsSet = new HashSet<>();
			try {
				while ((next = reader.readNext()) != null) {
					String source = next[0];

					ContentValues wordSource = new ContentValues();
					String wordKey = md5(sourceLang + source);
					wordSource.put(Word.FIELD_KEY, wordKey);
					wordSource.put(Word.FIELD_LANG, sourceLang);
					wordSource.put(Word.FIELD_TEXT, source);
					wordSource.put(Word.FIELD_CREATED, new Date().getTime());

					wordsSet.add(wordSource);

					String translationsStr = next[1];
					String[] translations = translationsStr.split(",");
					for (String translation : translations) {
						translation = translation.trim();

						ContentValues wordTranslation = new ContentValues();
						String translationKey = md5(translationLang + translation);
						wordTranslation.put(Word.FIELD_KEY, translationKey);
						wordTranslation.put(Word.FIELD_LANG, translationLang);
						wordTranslation.put(Word.FIELD_TEXT, translation);
						wordTranslation.put(Word.FIELD_CREATED, new Date().getTime());

						wordsSet.add(wordTranslation);

						ContentValues tr = new ContentValues();
						tr.put(Translation.FIELD_KEY1, wordKey);
						tr.put(Translation.FIELD_KEY2, translationKey);

						translationsSet.add(tr);
					}
				}

				wordManager.db.beginTransaction();
				try {
					for (ContentValues word : wordsSet) {
						wordManager.db.insertWithOnConflict(Word.TABLE_NAME, null, word, SQLiteDatabase.CONFLICT_IGNORE);
					}
					for (ContentValues tr : translationsSet) {
						wordManager.db.insertWithOnConflict(Translation.TABLE_NAME, null, tr, SQLiteDatabase.CONFLICT_IGNORE);
					}
					wordManager.db.setTransactionSuccessful();
				} finally {
					wordManager.db.endTransaction();
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

			words = wordManager.getUnlearntWords(Conveyor.CONVEYOR_LENGTH);
			ArrayList<String> keys = new ArrayList<>(words.size());
			for (WordDTO w : words) {
				keys.add(w.key);
			}
			head = 0;
			conveyorManager.persistConveyorAndHead(keys, head);

			SharedPreferences.Editor edit = preferences.edit();
			edit.putString(FIRST_RUN_KEY, "yes");
			edit.commit();
		} else {
			words = wordManager.getWordsForKeys(keysInConveyor);
			head = conveyorManager.getHead();
		}
		nativePushConveyorAndHead(words.toArray(new WordDTO[]{}), head);
	}

	public static String md5(String s) {
		String MD5 = "MD5";
		try {
			// Create MD5 Hash
			MessageDigest digest = java.security.MessageDigest
					.getInstance(MD5);
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuilder hexString = new StringBuilder();
			for (byte aMessageDigest : messageDigest) {
				String h = Integer.toHexString(0xFF & aMessageDigest);
				while (h.length() < 2)
					h = "0" + h;
				hexString.append(h);
			}
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	@Override
	protected void onResume() {
		super.onResume();
		nativeResume();
	}

	private void initManagers() {

		WordLearnOpenHelper helper = new WordLearnOpenHelper(getContext());

		SQLiteDatabase db = helper.getWritableDatabase();
		wordManager = new WordManager(db);
		conveyorManager = new ConveyorManager(db);
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	private void insertStubData() {

		ArrayList<ContentValues> words = new ArrayList<>();
		ArrayList<ContentValues> translations = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			ContentValues word1 = new ContentValues();
			String key = new String(new byte[]{(byte) ('a' + i)});
			word1.put(Word.FIELD_KEY, key);
			word1.put(Word.FIELD_TEXT, key + "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
			word1.put(Word.FIELD_LANG, Word.LANG_EN);

			ContentValues word2 = new ContentValues();
			String tr = key + "добрый день";
			word2.put(Word.FIELD_KEY, tr);
			word2.put(Word.FIELD_TEXT, tr);
			word2.put(Word.FIELD_LANG, Word.LANG_RU);

			ContentValues word3 = new ContentValues();
			String tr2 = key + "Здравствуйте";
			word3.put(Word.FIELD_KEY, tr2);
			word3.put(Word.FIELD_TEXT, tr2);
			word3.put(Word.FIELD_LANG, Word.LANG_RU);

			ContentValues translation = new ContentValues();
			translation.put(Translation.FIELD_KEY1, (String) word1.get(Word.FIELD_KEY));
			translation.put(Translation.FIELD_KEY2, (String) word2.get(Word.FIELD_KEY));

			ContentValues translation1 = new ContentValues();
			translation1.put(Translation.FIELD_KEY1, (String) word1.get(Word.FIELD_KEY));
			translation1.put(Translation.FIELD_KEY2, (String) word3.get(Word.FIELD_KEY));

			words.add(word1);
			words.add(word2);
			words.add(word3);

			translations.add(translation);
			translations.add(translation1);
		}

		try {
			wordManager.db.beginTransaction();

			for (ContentValues v : words) {
				wordManager.db.insert(Word.TABLE_NAME, null, v);
			}
			for (ContentValues v : translations) {
				wordManager.db.insert(Translation.TABLE_NAME, null, v);
			}

			ContentValues head = new ContentValues();
			head.put(ConveyorHead.FIELD_HEAD, 0);
			conveyorManager.db.insert(ConveyorHead.TABLE_NAME, null, head);

			wordManager.db.setTransactionSuccessful();
		} finally {
			wordManager.db.endTransaction();
		}
	}


	@Override
	protected void onDestroy() {
		wordManager.closeDb();
		nativeDestroyWords();
		super.onDestroy();
	}

	public static void persistConveyorAndHeadCallback(String[] wordKeys, int head) {
		Log.i("===", "===Head is: " + head);
		Log.i("===", "===conveyor is: " + Arrays.toString(wordKeys));

		conveyorManager.persistConveyorAndHead(Arrays.asList(wordKeys), head);
	}

	public static void persistWordCallback(WordDTO word) {
		Log.i("===", "===Persisting word: " + word);
		wordManager.updateWord(word);
	}

	public static WordDTO getUnlearntWordNotInConveyorCallback() {
		ArrayList<WordDTO> unlearntWords = wordManager.getUnlearntWords(1);
		if (unlearntWords.size() == 0) {
			return null;
		}
		return unlearntWords.get(0);
	}

	public static native void nativeDestroyWords();

	public static native void nativePushConveyorAndHead(WordDTO[] words, int head);

	public static native void nativeResume();

}