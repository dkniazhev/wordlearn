#ifndef _TABLE_
#define _TABLE_

#include "Word.h"

int WL_InitConveyor(struct Word **words, int count, int head);

int WL_Destroy();
/**
 * Main loop
 */
void WL_Main();

void WL_Resume();

#endif /** _TABLE_ */