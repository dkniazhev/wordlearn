#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <assert.h>

#include "Table.h"
#include "TableWrapper.h"

#include "SDL.h"
#include "SDL_ttf.h"
#include "SDL_image.h"

#define MAX_FAIL_SHIFT 30
#define MIN_FAIL_SHIFT 20

#define POSITIVE_ATTEMPTS_LIMIT 10

#define char_size sizeof(char)

//todo volatile?
struct Word **conveyor;
int conveyorLength = 0;
//todo head
int wordIndex = 0;

int needsRendering = 1;
int first = 1;

char* conveyorToString() {
	size_t size = 0;
	int i = 0;
	for(; i < conveyorLength; i++) {
		struct Word *w = conveyor[i];
		size += char_size * strlen(w->source);
	}
	//space for comma-separated string
	char *str = malloc(size + (i)*char_size);
	str[0] = '\0';
	i = 0;
	for(; i <conveyorLength; i++) {
		struct Word *w = conveyor[i];
        strcat(str, w->source);
        strcat(str, ",");
	}

	return str;
}

int WL_InitConveyor(struct Word **words, int count, int head) {

	conveyor = words;
	wordIndex = head;
	conveyorLength = count;

//	SDL_Log("=====Conveyor: %s", conveyorToString());

	return 0;
}

void freeWord(struct Word *w) {
	free(w->source);
	free(w->translation);
	free(w);
}

int WL_Destroy() {
	while(--conveyorLength >= 0) {
		struct Word *w = conveyor[conveyorLength];
		freeWord(w);
		SDL_Log("==Word destroyed");
	}
	free(conveyor);
	SDL_Log("==All words destroyed");
}

void WL_Resume() {
	needsRendering = 1;
	first = 1;
}

void shiftConveyer(int offset) {
	if(conveyorLength == 0) {
		return;
	}
    if(! offset) {
    	//success, increment the index
    	wordIndex = (wordIndex + 1) % conveyorLength;
	} else {
		struct Word *currentWord = conveyor[wordIndex];

		int nextPosition = wordIndex + offset;

		int i;
		for(i = wordIndex; i < nextPosition; i++) {
			conveyor[i % conveyorLength] = conveyor[(i + 1) % conveyorLength];
		}

		conveyor[nextPosition % conveyorLength] = currentWord;
	}
}

typedef struct Sprite
{
	SDL_Texture* texture;
	Uint16 w;
	Uint16 h;
} Sprite;

/* Adapted from SDL's testspriteminimal.c */
Sprite LoadSprite(const char* file, SDL_Renderer* renderer)
{
	Sprite result;
	result.texture = NULL;
	result.w = 0;
	result.h = 0;

    SDL_Surface* temp;

    /* Load the sprite image */
    temp = IMG_Load(file);
    if (temp == NULL)
	{
        SDL_Log("Couldn't load %s: %s\n", file, SDL_GetError());
        return result;
    }
    result.w = temp->w;
    result.h = temp->h;

    /* Create texture from the image */
    result.texture = SDL_CreateTextureFromSurface(renderer, temp);
    if (!result.texture) {
        SDL_Log("=================Couldn't create texture: %s\n", SDL_GetError());
        SDL_FreeSurface(temp);
        return result;
    }
    SDL_FreeSurface(temp);

    return result;
}

//copied from http://stackoverflow.com/a/9210560/369317
char** str_split(char* a_str, const char a_delim)
{
    char** result    = 0;
    size_t count     = 0;
    char* tmp        = a_str;
    char* last_comma = 0;
    char delim[2];
    delim[0] = a_delim;
    delim[1] = 0;

    /* Count how many elements will be extracted. */
    while (*tmp)
    {
        if (a_delim == *tmp)
        {
            count++;
            last_comma = tmp;
        }
        tmp++;
    }

    /* Add space for trailing token. */
    count += last_comma < (a_str + strlen(a_str) - 1);

    /* Add space for terminating null string so caller
       knows where the list of returned strings ends. */
    count++;

    result = malloc(sizeof(char*) * count);

    if (result)
    {
        size_t idx  = 0;
        char* token = strtok(a_str, delim);

        while (token)
        {
            assert(idx < count);
            *(result + idx++) = strdup(token);
            token = strtok(0, delim);
        }
        assert(idx == count - 1);
        *(result + idx) = 0;
    }

    return result;
}
void WL_Main() {
	SDL_Window *window;
	SDL_Renderer *renderer;

	if(SDL_CreateWindowAndRenderer(0, 0, 0, &window, &renderer) < 0)
		exit(2);

	Sprite sprite = LoadSprite("image.bmp", renderer);
	if(sprite.texture == NULL)
		exit(2);

	if(!TTF_WasInit() && TTF_Init()==-1) {
		printf("TTF_Init: %s\n", TTF_GetError());
		exit(1);
	}
	int  windowWidth, windowHeight;
	SDL_GetWindowSize(window, &windowWidth, &windowHeight);

	TTF_Font *font;
	//todo add fonts for different languages (CJK first)
	font=TTF_OpenFont("newathu405.ttf", 100);
	if(!font) {
		printf("TTF_OpenFont: %s\n", TTF_GetError());
		exit(2);
	}


	SDL_Color redColor={232, 169, 169};
	SDL_Color greenColor={169, 232, 190};
	SDL_Color whiteColor={255, 255, 255};

	Sprite leftArrow = LoadSprite("arrow_left.bmp", renderer);
	int arrowSide = 200;
	SDL_Rect leftArrowRect = {0, windowHeight/4*3, arrowSide, arrowSide};
	SDL_Rect rightArrowRect = {windowWidth - arrowSide, windowHeight/4*3, arrowSide, arrowSide};

	struct Word *currentWord;

	int translationClicked = 0;

	//todo constants
	int success = 0;
	int fail = 0;

	while(1)
	{
		//todo frame manager, ->60fps
		SDL_Delay(300);
		if(conveyorLength == 0) {
			continue;
		}

		SDL_Event event;

		/* Check for events */
		while(SDL_PollEvent(&event))
		{
			switch(event.type) {
				//todo finger event handling
				case SDL_MOUSEBUTTONDOWN: {
					if(success || fail) {
						//translation is shown on the screen
						SDL_Log("==Translation on the screen clicked");
						translationClicked = 1;
						needsRendering = 1;
					} else {
						SDL_Point point = {event.button.x, event.button.y};
						if(SDL_EnclosePoints(&point, 1, &leftArrowRect, NULL)) {
							fail = 1;
							SDL_Log("=======Word is unknown");
						} else if (SDL_EnclosePoints(&point, 1, &rightArrowRect, NULL)) {
							SDL_Log("=======Word is known");
							success = 1;
						}
						if(success || fail) {
							needsRendering = 1;
						}
					}
					break;
				};
			}
		}
		if(needsRendering && (first || success || fail || translationClicked)) {
			needsRendering = 0;
			SDL_Log("===Rendering");
//			SDL_Log("=====Conveyor:%s, head is %d", conveyorToString(), wordIndex);

			if(translationClicked) {
				SDL_Log("==Translation clicked, shifting conveyor and stuff...");
				int offset;
				if(success) {
					offset = 0;
					currentWord->positiveAttempts++;
					if(currentWord->positiveAttempts - currentWord->negativeAttempts >= POSITIVE_ATTEMPTS_LIMIT) {
						currentWord->learnt = 1;
						WRAPPER_PersistWord(currentWord);
						freeWord(currentWord);
						struct Word *nextWord = WRAPPER_GetUnlearntWord();
						if(nextWord == NULL) {
							//all words have been learnt, shrink the conveyor
							int i;
							for(i = wordIndex; i < conveyorLength - 1; i++) {
								conveyor[i] = conveyor[i + 1];
							}

							conveyorLength--;
						} else {
							//place new word instead of the learnt one and move on
							currentWord = nextWord;
							conveyor[wordIndex] = nextWord;
						}
					} else {
						WRAPPER_PersistWord(currentWord);
					}
				} else if (fail) {
					offset = MIN_FAIL_SHIFT + rand() % (MAX_FAIL_SHIFT - MIN_FAIL_SHIFT + 1);
					currentWord->negativeAttempts++;
					WRAPPER_PersistWord(currentWord);
				}
				shiftConveyer(offset);
				WRAPPER_PersistConveyor(conveyor, conveyorLength, wordIndex);

				success = 0;
				fail = 0;
			}
			char *text;
			SDL_Color color = whiteColor;

			if(conveyorLength == 0) {
				SDL_Log("==All words have been learnt");
				text = "=Все слова выучены=";
				translationClicked = 0;
			} else {
				currentWord = conveyor[wordIndex];
			}


			if(success || fail) {
				SDL_Log("==Showing translation");
				text = currentWord->translation;
				if(success) {
					color = greenColor;
				} else if (fail) {
					color = redColor;
				}
			} else if(first || translationClicked) {
				first = 0;
				translationClicked = 0;
				SDL_Log("==Showing the word");
				text = currentWord->source;
				color = whiteColor;
			}
			/* Draw a black background */
			SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
			SDL_RenderClear(renderer);

			SDL_RenderCopy(renderer, leftArrow.texture, NULL, &leftArrowRect);
			SDL_RenderCopyEx(renderer, leftArrow.texture, NULL, &rightArrowRect, 0, NULL, SDL_FLIP_HORIZONTAL);

			char *textDup = strdup(text);
			char **tokens = str_split(textDup, ',');
			free(textDup);

			int i;
			for(i = 0; *(tokens + i); i++) {
				SDL_Surface *text_surface = TTF_RenderUTF8_Solid(font, *(tokens + i), color);

				int wordWidth = text_surface->w, wordHeight = text_surface->h;
				SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, text_surface);
				SDL_FreeSurface(text_surface);

				SDL_Rect destRect = {0, 0 + wordHeight*i, windowWidth, wordHeight};

				SDL_RenderCopy(renderer, texture, NULL, &destRect);

				SDL_DestroyTexture(texture);

				free(*(tokens + i));
			}

			free(tokens);
			SDL_RenderPresent(renderer);

		}
	}

	SDL_DestroyTexture(leftArrow.texture);

	//todo exit properly
	exit(0);
}