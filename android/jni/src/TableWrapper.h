#ifndef _TABLE_WRAPPER_
#define _TABLE_WRAPPER_

void WRAPPER_PersistConveyor(struct Word **conveyor, int count, int head);

void WRAPPER_PersistWord(struct Word *word);

struct Word* WRAPPER_GetUnlearntWord();

#endif /** _TABLE_WRAPPER_ */