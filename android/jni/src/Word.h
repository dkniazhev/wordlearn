#ifndef _WORD_
#define _WORD_

struct Word
{
	char* key;
	char* source;
	char* translation;
	int positiveAttempts;
	int negativeAttempts;
	int **badgeIds;
	int learnt;
};

#endif /** _WORD_*/