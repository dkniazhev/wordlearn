#include "SDL.h"
#include "Table.h"
#include "TableWrapper.h"
#include <string.h>
#include <jni.h>

JavaVM *jvm;

jclass getWordDtoClass(JNIEnv *env);
jclass getTableActivityClass(JNIEnv *env);
struct Word *getWordFromJObject(jobject word, JNIEnv* env);

void JNICALL Java_com_denisk_wordlearn_TableActivity_nativePushConveyorAndHead
  (JNIEnv *env, jclass class, jobjectArray wordsObjectArray, jint head) {
	int count = (*env)->GetArrayLength(env, wordsObjectArray);

	struct Word **words = malloc(count * sizeof(struct Word*));

	int i;
	for(i = 0; i < count; i++) {
		jobject word = (*env)->GetObjectArrayElement(env, wordsObjectArray, i);
		words[i] = getWordFromJObject(word, env);
	}

	WL_InitConveyor(words, count, head);

	//save JVM pointer
	(*env)->GetJavaVM(env, &jvm);
}

void JNICALL Java_com_denisk_wordlearn_TableActivity_nativeDestroyWords
(JNIEnv *env, jclass class) {
	WL_Destroy();
}

void JNICALL Java_com_denisk_wordlearn_TableActivity_nativeResume
(JNIEnv *env, jclass class) {
	WL_Resume();
}

JNIEnv* getEnv() {
	JNIEnv *env;
	(*jvm)->AttachCurrentThread(jvm, &env, NULL);
	return env;
}

struct Word *getWordFromJObject(jobject word, JNIEnv* env) {
	if(word == NULL) {
		return NULL;
	}

	//todo this should be obtained once, not for every word!!!
	jclass wordClass = getWordDtoClass(env);

	jfieldID keyField = (*env)->GetFieldID(env, wordClass, "key", "Ljava/lang/String;");
	jfieldID sourceField = (*env)->GetFieldID(env, wordClass, "text", "Ljava/lang/String;");
	jfieldID translationField = (*env)->GetFieldID(env, wordClass, "translation", "Ljava/lang/String;");
	jfieldID positiveAttemptsField = (*env)->GetFieldID(env, wordClass, "positiveAttempts", "I");
	jfieldID negativeAttemptsField = (*env)->GetFieldID(env, wordClass, "negativeAttempts", "I");
	jfieldID learntField = (*env)->GetFieldID(env, wordClass, "learnt", "I");

	const jstring keyString =  (*env)->GetObjectField(env, word, keyField);
	const jstring sourceString =  (*env)->GetObjectField(env, word, sourceField);
	const jstring translationString =  (*env)->GetObjectField(env, word, translationField);
	const jint positiveAttempts = (*env)->GetIntField(env, word, positiveAttemptsField);
	const jint negativeAttempts = (*env)->GetIntField(env, word, negativeAttemptsField);
	const jint learnt = (*env)->GetIntField(env, word, learntField);

	const char *key = (*env)->GetStringUTFChars(env, keyString, NULL);
	const char *source = (*env)->GetStringUTFChars(env, sourceString, NULL);
	const char *translation = (*env)->GetStringUTFChars(env, translationString, NULL);

	struct Word *w = malloc(sizeof(struct Word));

	w->key = strdup(key);
	w->source = strdup(source);
	w->translation = strdup(translation);
	w->positiveAttempts = positiveAttempts;
	w->negativeAttempts = negativeAttempts;
	w->learnt = learnt;

	//cleanup
	(*env)->ReleaseStringUTFChars(env, keyString, key);
	(*env)->ReleaseStringUTFChars(env, sourceString, source);
	(*env)->ReleaseStringUTFChars(env, translationString, translation);

	return w;
}

void WRAPPER_PersistConveyor(struct Word **conveyor, int conveyorLength, int head) {
	JNIEnv *env = getEnv();

	(*env)->PushLocalFrame(env, 30);

	jclass activityClass = getTableActivityClass(env);
	jmethodID persistConveyorMethod = (*env)->GetStaticMethodID(env, activityClass, "persistConveyorAndHeadCallback", "([Ljava/lang/String;I)V");

	jobjectArray keysArray = (*env)->NewObjectArray(env, conveyorLength, (*env)->FindClass(env, "java/lang/String"), NULL);

	int i = 0;
	for(; i < conveyorLength; i++) {
		struct Word *w = conveyor[i];
		jstring key = (*env)->NewStringUTF(env, w->key);
		(*env)->SetObjectArrayElement(env, keysArray, i, key);
	}

	(*env)->CallStaticVoidMethod(env, activityClass, persistConveyorMethod, keysArray, head);

	SDL_Log("==Persisting conveyor");

	(*env)->PopLocalFrame(env, NULL);

}

void WRAPPER_PersistWord(struct Word *word){
	JNIEnv *env = getEnv();

	(*env)->PushLocalFrame(env, 30);

	jclass activityClass = getTableActivityClass(env);
	jmethodID persistWordMethod = (*env)->GetStaticMethodID(env, activityClass, "persistWordCallback", "(Lcom/denisk/wordlearn/dto/WordDTO;)V");
	
	jclass wordDtoClass = getWordDtoClass(env);

	jmethodID wordDtoConstructor = (*env)->GetMethodID(env, wordDtoClass, "<init>", "(Ljava/lang/String;III)V");

	jobject wordDto = (*env)->NewObject(env, wordDtoClass, wordDtoConstructor, (*env)->NewStringUTF(env, word->key), word->positiveAttempts, word->negativeAttempts, word->learnt);

	(*env)->CallStaticVoidMethod(env, activityClass, persistWordMethod, wordDto);

	(*env)->PopLocalFrame(env, NULL);

}

jclass getWordDtoClass(JNIEnv *env) {
	return (*env)->FindClass(env, "com/denisk/wordlearn/dto/WordDTO");
}

jclass getTableActivityClass(JNIEnv *env) {
	return (*env)->FindClass(env, "com/denisk/wordlearn/TableActivity");
}

struct Word* WRAPPER_GetUnlearntWord(){

	JNIEnv *env = getEnv();

	//todo this and all other push/pop local frame calls should be centralized
	(*env)->PushLocalFrame(env, 30);

	jclass activityClass = getTableActivityClass(env);
	jmethodID getWordMethod = (*env)->GetStaticMethodID(env, activityClass, "getUnlearntWordNotInConveyorCallback", "()Lcom/denisk/wordlearn/dto/WordDTO;");
	jobject wordDto = (*env)->CallStaticObjectMethod(env, activityClass, getWordMethod);

	struct Word* result = NULL;
	if(wordDto == NULL) {
		SDL_Log("***Null wordDTO");
	} else {
		SDL_Log("***Not NULL wordDTO");
		result = getWordFromJObject(wordDto, env);
	}

	(*env)->PopLocalFrame(env, NULL);

	return result;
}

int main(int argc, char *argv[]) {
	WL_Main();
}
